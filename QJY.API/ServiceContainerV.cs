﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Unity;
using QJY.Data;

namespace QJY.API
{
    public class ServiceContainerV
    {
        public static IUnityContainer Current()
        {

            IUnityContainer container = new UnityContainer();





            //免注册接口类
            container.RegisterType<IWsService, Commanage>("Commanage".ToUpper());//


            #region 基础模块接口

            //基础接口
            container.RegisterType<IWsService, AuthManage>("XTGL".ToUpper());//
            container.RegisterType<IWsService, INITManage>("INIT".ToUpper());//系统配置相关API



            #endregion

            #region 信息发布
            container.RegisterType<IWsService, XXFBManage>("XXFB");



            #endregion

            #region 出差休假
            container.RegisterType<IWsService, CCXJManage>("CCXJ".ToUpper());//根据部门获取用户列表

            #endregion

            #region 流程审批
            container.RegisterType<IWsService, LCSPManage>("LCSP".ToUpper());//


            #endregion

            #region JSAPI
            container.RegisterType<IWsService, JSAPI>("JSSDK".ToUpper());
            #endregion

            #region 短信管理
            container.RegisterType<IWsService, DXGLManage>("DXGL".ToUpper());//删除短信 
            #endregion

            #region 通讯录
            container.RegisterType<IWsService, TXLManage>("QYTX".ToUpper());//通讯录 
            #endregion

            #region 提醒事项
            container.RegisterType<IWsService, TXSXManage>("TXSX".ToUpper());//删除短信 

            #endregion

            #region 文档管理
            container.RegisterType<IWsService, QYWDManage>("QYWD".ToUpper());//企业文档 

            #endregion

            //记事本
            container.RegisterType<IWsService, NOTEManage>("NOTE".ToUpper());//记事本管理 
            container.RegisterType<IWsService, DBGLManage>("DBGL".ToUpper());//数据库管理
            container.RegisterType<IWsService, JHZDManage>("JHZD".ToUpper());
            container.RegisterType<IWsService, JHGLManage>("JHGL".ToUpper());
            container.RegisterType<IWsService, JHSRManage>("JHSR".ToUpper());
            container.RegisterType<IWsService, JHZCManage>("JHZC".ToUpper());
            container.RegisterType<IWsService, JHZXManage>("JHZX".ToUpper());
            container.RegisterType<IWsService, ZCZXManage>("ZCZX".ToUpper());

            container.RegisterType<IWsService, JHBGManage>("JHBG".ToUpper());

            container.RegisterType<IWsService, SJTJManage>("SJTJ".ToUpper());

            return container;
        }

    }
}
