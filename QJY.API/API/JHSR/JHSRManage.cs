using FastReflectionLib;
using Newtonsoft.Json;
using QJY.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;

namespace QJY.API
{
    public class JHSRManage : IWsService
    {

        public void ProcessRequest(HttpContext context, ref Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            MethodInfo methodInfo = typeof(JHSRManage).GetMethod(msg.Action.ToUpper());
            JHSRManage model = new JHSRManage();
            methodInfo.FastInvoke(model, new object[] { context, msg, P1, P2, UserInfo });
        }


        /// <summary>
        /// 获取计划列表
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETLIST_PAGE(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strWhere = string.Format(" SZHL_JH.ComId=" + UserInfo.User.ComId + " AND SZHL_JH.JHStatus<>'-1'");
            if (P1 != "") //图书码
            {
                strWhere += string.Format("and ( SZHL_JH.JHTitle like '%{0}%' OR  SZHL_JH.BranchName like '%{0}%' )", P1);
            }
            string JHStatus = context.Request["JHStatus"] ?? "";
            if (JHStatus != "")//图书类型
            {
                strWhere += string.Format(" And SZHL_JH.JHStatus='{0}'", JHStatus);
            }
            int recordCount = 0;
            int page = 0;
            int pagecount = 8;
            int.TryParse(context.Request["p"] ?? "1", out page);
            int.TryParse(context.Request["pagecount"] ?? "8", out pagecount);//页数
            DataTable dt = new SZHL_JHB().GetDataPager(" SZHL_JH ", "SZHL_JH.*", pagecount, page, "SZHL_JH.CRDate desc", strWhere, ref recordCount);
            msg.Result = dt;
            msg.Result1 = recordCount;
        }




        /// <summary>
        /// 获取计划明细
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETJHMXLIST(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int Id = int.Parse(P1);
            msg.Result = new VwSRB().GetEntities(d => d.JHID == Id && d.CRUser == UserInfo.User.UserName && d.status == P2);
        }





        #region 计划明细管理


        public void GETMODEL(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int Id = int.Parse(P1);
            SZHL_JH_ITEMSR MODEL = new SZHL_JH_ITEMSRB().GetEntity(d => d.ID == Id);
            msg.Result = MODEL;


        }




        public void GETMXINITDATA(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int Id = int.Parse(P1);
            SZHL_JH MODEL = new SZHL_JHB().GetEntity(D => D.ID == Id);
            msg.Result = new SZHL_JHB().GetEntities(d => d.ID == Id);
            msg.Result1 = new JH_Auth_ZiDianB().GetEntities(d => d.Class == 28 && d.Remark != "1" && d.TypeNO == UserInfo.User.BranchCode.ToString());//项目期别
            msg.Result2 = new JH_Auth_ZiDianB().GetEntities(d => d.Class == 27 && d.Remark != "1");//回款类型
            msg.Result3 = new JH_Auth_ZiDianB().GetEntities(d => d.Class == 29 && d.Remark != "1");//产品类型
            msg.Result4 = UserInfo.BranchInfo;



        }




        public void GETMXMQB(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            msg.Result1 = new JH_Auth_ZiDianB().GetEntities(d => d.Class == 28 && d.Remark != "1" && d.TypeNO == P1);//项目期别



        }

        /// <summary>
        /// 添加计划明细--收入
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1">客户信息</param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void ADDJHMXSR(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            SZHL_JH_ITEMSR MODEL = JsonConvert.DeserializeObject<SZHL_JH_ITEMSR>(P1);


            if (MODEL.ID == 0)
            {
                MODEL.CRDate = DateTime.Now;
                MODEL.CRUser = UserInfo.User.UserName;
                MODEL.ComID = UserInfo.User.ComId;
                new SZHL_JH_ITEMSRB().Insert(MODEL);
            }
            else
            {
                new SZHL_JH_ITEMSRB().Update(MODEL);
            }
            msg.Result = MODEL;
        }






        public void DEL(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int[] ListData = P1.TrimEnd(',').SplitTOInt(',');
            new SZHL_JH_ITEMSRB().Delete(d => ListData.Contains(d.ID));
        }





        public void TIANBI(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int[] ListData = P1.TrimEnd(',').SplitTOInt(',');

            JH_Auth_QY_Model Model = new JH_Auth_QY_ModelB().GetEntity(d => d.QYModelCode == "JHSR");
            int PDID = 0;
            int.TryParse(Model.PDID.ToString(), out PDID);
            Yan_WF_PD PD = new Yan_WF_PDB().GetEntity(d => d.ID == PDID && d.ComId == UserInfo.User.ComId);
            Yan_WF_PIB PIB = new Yan_WF_PIB();
            List<string> ListNextUser = new List<string>();//获取下一任务的处理人
            foreach (int DataID in ListData)
            {
                Yan_WF_TI TI = PIB.StartWF(PD, "JHSR", UserInfo.User.UserName, "", "", ref ListNextUser);
                //更新关联表的流程ID

                SZHL_JH_ITEMSR SR = new SZHL_JH_ITEMSRB().GetEntity(d => d.ID == DataID);
                SR.intProcessStanceid = TI.PIID;
                SR.status = "1";
                SR.statusremark = "";
                new SZHL_JH_ITEMSRB().Update(SR);


            }

        }

        /// <summary>
        /// 获取审核的收入计划
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETSHMXLIST(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strSQL = string.Format(" SELECT * FROM VwSR WHERE JHID='{0}' ", P1);
            List<string> intProD = new List<string>();
            if (P2 == "0")//待审核
            {
                intProD = new Yan_WF_PIB().GetDSH(UserInfo.User).Select(d => d.PIID.ToString()).ToList();
            }
            else
            {
                intProD = new Yan_WF_PIB().GetYSH(UserInfo.User).Select(d => d.PIID.ToString()).ToList();

            }
            strSQL += " And VwSR.dwid in ('" + UserInfo.UserBMQXCode.ToFormatLike(',') + "')  AND    VwSR.intProcessStanceid in (" + (intProD.ListTOString(',') == "" ? "-1" : intProD.ListTOString(',')) + ")";
            msg.Result = new SZHL_JH_ITEMSRB().GetDTByCommand(strSQL);

        }





        /// <summary>
        /// 审核收入计划
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void SHSR(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int[] ListData = P1.TrimEnd(',').SplitTOInt(',');
            foreach (int DataID in ListData)
            {
                SZHL_JH_ITEMSR SR = new SZHL_JH_ITEMSRB().GetEntity(d => d.ID == DataID);
                Yan_WF_PIB PIB = new Yan_WF_PIB();
                List<string> ListNextUser = new List<string>();
                PIB.MANAGEWF(UserInfo.User.UserName, SR.intProcessStanceid.Value, "审核通过", ref ListNextUser, "");//处理任务
                string strIsComplete = ListNextUser.Count() == 0 ? "Y" : "N";//结束流程,找不到人了
                if (strIsComplete == "Y")//找不到下家就结束流程,并且给流程发起人发送消息
                {
                    PIB.ENDWF(SR.intProcessStanceid.Value);
                    SR.status = "2";
                    new SZHL_JH_ITEMSRB().Update(SR);

                }
            }
        }


        /// <summary>
        /// 退回计划
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void THSR(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int[] ListData = P1.TrimEnd(',').SplitTOInt(',');
            foreach (int DataID in ListData)
            {
                SZHL_JH_ITEMSR SR = new SZHL_JH_ITEMSRB().GetEntity(d => d.ID == DataID);
                Yan_WF_PIB PIB = new Yan_WF_PIB();
                List<string> ListNextUser = new List<string>();
                new Yan_WF_PIB().REBACKLC(UserInfo.User.UserName, SR.intProcessStanceid.Value, P2);//退回任务
                SR.status = "-1";
                SR.statusremark = P2;

                new SZHL_JH_ITEMSRB().Update(SR);

            }
        }





        #endregion



    }
}