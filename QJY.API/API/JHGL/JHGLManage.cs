﻿using FastReflectionLib;
using Newtonsoft.Json;
using QJY.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;

namespace QJY.API
{
    public class JHGLManage : IWsService
    {

        public void ProcessRequest(HttpContext context, ref Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            MethodInfo methodInfo = typeof(JHGLManage).GetMethod(msg.Action.ToUpper());
            JHGLManage model = new JHGLManage();
            methodInfo.FastInvoke(model, new object[] { context, msg, P1, P2, UserInfo });
        }








        #region 计划管理

        /// <summary>
        /// 获取计划列表
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETLIST_PAGE(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strWhere = string.Format(" SZHL_JH.ComId=" + UserInfo.User.ComId + " AND SZHL_JH.JHStatus<>'-1'");
            if (P1 != "") //
            {
                strWhere += string.Format("and ( SZHL_JH.JHTitle like '%{0}%' OR  SZHL_JH.BranchName like '%{0}%' )", P1);
            }
            string JHStatus = context.Request["JHStatus"] ?? "";
            if (JHStatus != "")//
            {
                strWhere += string.Format(" And SZHL_JH.JHStatus='{0}'", JHStatus);
            }
            int recordCount = 0;
            int page = 0;
            int pagecount = 8;
            int.TryParse(context.Request["p"] ?? "1", out page);
            int.TryParse(context.Request["pagecount"] ?? "8", out pagecount);//页数
            DataTable dt = new SZHL_JHB().GetDataPager(" SZHL_JH ", "SZHL_JH.*", pagecount, page, "SZHL_JH.CRDate desc", strWhere, ref recordCount);
            msg.Result = dt;
            msg.Result1 = recordCount;
        }





        /// <summary>
        /// 获取最新的已打开的计划
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETNOWJH(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            SZHL_JH MODEL = new SZHL_JHB().GetEntities(d => d.JHStatus == "0").FirstOrDefault();
            msg.Result = MODEL;


        }
        /// <summary>
        /// 获取出差请假信息
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETMODEL(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int Id = int.Parse(P1);
            SZHL_JH MODEL = new SZHL_JHB().GetEntity(d => d.ID == Id);
            msg.Result = MODEL;


        }





        public void DELJH(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int Id = int.Parse(P1);
            SZHL_JH MODEL = new SZHL_JHB().GetEntity(d => d.ID == Id);
            MODEL.JHStatus = "-1";
            new SZHL_JHB().Update(MODEL);
            msg.Result = MODEL;


        }
        public void CLOSEJH(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int Id = int.Parse(P1);
            SZHL_JH MODEL = new SZHL_JHB().GetEntity(d => d.ID == Id);
            MODEL.JHStatus = "1";
            new SZHL_JHB().Update(MODEL);
            msg.Result = MODEL;


        }




        /// <summary>
        /// 添加出差请假
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1">客户信息</param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void ADDJHGL(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            SZHL_JH MODEL = JsonConvert.DeserializeObject<SZHL_JH>(P1);

            if (string.IsNullOrEmpty(MODEL.JHTitle))
            {
                msg.ErrorMsg = "计划名称不能为空";
                return;
            }
            if (MODEL.ID == 0)
            {
                MODEL.ShenQingRen = UserInfo.User.UserRealName;
                MODEL.CRDate = DateTime.Now;
                MODEL.CRUser = UserInfo.User.UserName;
                MODEL.ComId = UserInfo.User.ComId;
                new SZHL_JHB().Insert(MODEL);
            }
            else
            {
                new SZHL_JHB().Update(MODEL);
            }
            msg.Result = MODEL;
        }
        #endregion





        #region 计划明细查看

        public void GETCKINITDATA(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            msg.Result1 = new JH_Auth_ZiDianB().GetEntities(d => d.Class == 28 && d.Remark != "1" && d.TypeNO == UserInfo.User.BranchCode.ToString());//项目期别
            msg.Result2 = new JH_Auth_ZiDianB().GetEntities(d => d.Class == 27 && d.Remark != "1");//回款类型
            msg.Result3 = new JH_Auth_ZiDianB().GetEntities(d => d.Class == 29 && d.Remark != "1");//产品类型
            msg.Result4 = UserInfo.BranchInfo;



        }

        /// <summary>
        /// 获取计划明细
        /// </summary>
        /// <param name="context"></param>5
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETJHSRMXLIST(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int Id = int.Parse(P1);
            string dw = context.Request["dw"] ?? "";
            string jhkm = context.Request["jhkm"] ?? "";
            string cplx = context.Request["cplx"] ?? "";
            string hklx = context.Request["hklx"] ?? "";

            string zxje1 = context.Request["zxje1"] ?? "";
            string zxje2 = context.Request["zxje2"] ?? "";



            if (dw == "")
            {
                dw = UserInfo.UserBMQXCode;
            }
            string strSQLWhere = "SELECT *  FROM   VwSR WHERE JHID='" + Id + "' and dwid in ('" + dw.ToFormatLike(',') + "')";

            if (jhkm != "")
            {
                strSQLWhere = strSQLWhere + " AND path like '" + jhkm + "%'";
            }


            if (cplx != "")
            {
                strSQLWhere = strSQLWhere + " AND cplxid ='" + cplx + "'";
            }
            if (hklx != "")
            {
                strSQLWhere = strSQLWhere + " AND hklxid ='" + hklx + "'";
            }

            if (zxje1 != "")
            {
                strSQLWhere = strSQLWhere + " AND je1 > '" + zxje1 + "'";
            }
            if (zxje2 != "")
            {
                strSQLWhere = strSQLWhere + " AND je1 < '" + zxje2 + "'";
            }

            if (P2 == "")
            {
                strSQLWhere = strSQLWhere + " AND status != '0'";

            }
            else
            {
                strSQLWhere = strSQLWhere + " AND status = '" + P2 + "'";
            }

            msg.Result = new VwSRB().GetDTByCommand(strSQLWhere);


        }


        public void GETJHZCMXLIST(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            int Id = int.Parse(P1);
            string dw = context.Request["dw"] ?? "";
            string jhkm = context.Request["jhkm"] ?? "";
            string fkjc = context.Request["fkjc"] ?? "";
            string dfdw = context.Request["dfdw"] ?? "";
            string qpzt = context.Request["qpzt"] ?? "";
            string htbh = context.Request["htbh"] ?? "";
            string zxje1 = context.Request["zxje1"] ?? "";
            string zxje2 = context.Request["zxje2"] ?? "";



            if (dw == "")
            {
                dw = UserInfo.UserBMQXCode;
            }
            string strSQLWhere = "SELECT *  FROM   VwZC WHERE JHID='" + Id + "' and dwid in ('" + dw.ToFormatLike(',') + "')";

            if (jhkm != "")
            {
                strSQLWhere = strSQLWhere + " AND path like '" + jhkm + "%'";
            }
            if (fkjc != "")
            {
                strSQLWhere = strSQLWhere + " AND fkjcid ='" + fkjc + "'";
            }
            if (dfdw != "")
            {
                strSQLWhere = strSQLWhere + " AND dfdw like '%" + dfdw + "%'";
            }
            if (qpzt != "")
            {
                strSQLWhere = strSQLWhere + " AND htqpzt ='" + qpzt + "'";
            }
            if (htbh != "")
            {
                strSQLWhere = strSQLWhere + " AND htbh like '%" + htbh + "%'";
            }

            if (zxje1 != "")
            {
                strSQLWhere = strSQLWhere + " AND je1 > '" + zxje1 + "'";
            }
            if (zxje2 != "")
            {
                strSQLWhere = strSQLWhere + " AND je1 < '" + zxje2 + "'";
            }

            if (P2 == "")
            {
                strSQLWhere = strSQLWhere + " AND status != '0'";

            }
            else
            {
                strSQLWhere = strSQLWhere + " AND status == '" + P2 + "'";
            }

            msg.Result = new VwSRB().GetDTByCommand(strSQLWhere);

        }
        #endregion




        #region 计划控制






        /// <summary>
        /// 获取控制计划的明细
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETKZMXLIST(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int Id = int.Parse(P1);
            msg.Result = new VwSRB().GetEntities(d => d.JHID == Id && d.status == P2);


        }





        public void GETKZZCMXLIST(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int Id = int.Parse(P1);
            msg.Result = new VwZCB().GetEntities(d => d.JHID == Id && d.status == P2);


        }


        /// <summary>
        /// 上报数据
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void SBSR(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int[] ListData = P1.TrimEnd(',').SplitTOInt(',');
            foreach (int DataID in ListData)
            {
                SZHL_JH_ITEMSR SR = new SZHL_JH_ITEMSRB().GetEntity(d => d.ID == DataID);
                if (SR.status == "2")
                {
                    SR.status = "3";
                    new SZHL_JH_ITEMSRB().Update(SR);
                }

            }
        }



        /// <summary>
        /// 中止收入计划
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void ZZSR(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int[] ListData = P1.TrimEnd(',').SplitTOInt(',');
            foreach (int DataID in ListData)
            {
                SZHL_JH_ITEMSR SR = new SZHL_JH_ITEMSRB().GetEntity(d => d.ID == DataID);
                if (SR.status == "3")
                {
                    SR.status = "4";
                    new SZHL_JH_ITEMSRB().Update(SR);
                }

            }
        }

        /// <summary>
        /// 恢复上报状态
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void HFSR(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int[] ListData = P1.TrimEnd(',').SplitTOInt(',');
            foreach (int DataID in ListData)
            {
                SZHL_JH_ITEMSR SR = new SZHL_JH_ITEMSRB().GetEntity(d => d.ID == DataID);
                if (SR.status == "4")
                {
                    SR.status = "3";
                    new SZHL_JH_ITEMSRB().Update(SR);
                }

            }
        }





        /// <summary>
        /// 上报数据
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void SBZC(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int[] ListData = P1.TrimEnd(',').SplitTOInt(',');
            foreach (int DataID in ListData)
            {
                SZHL_JH_ITEMZC ZC = new SZHL_JH_ITEMZCB().GetEntity(d => d.ID == DataID);
                if (ZC.status == "2")
                {
                    //如果不需要上报，直接进入执行状态
                    ZC.status = "3";
                    if (ZC.remark2 == "否")
                    {
                        ZC.status = "8";
                    }
                    new SZHL_JH_ITEMZCB().Update(ZC);
                }

            }
        }



        /// <summary>
        /// 中止收入计划
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void ZZZC(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int[] ListData = P1.TrimEnd(',').SplitTOInt(',');
            foreach (int DataID in ListData)
            {
                SZHL_JH_ITEMZC SR = new SZHL_JH_ITEMZCB().GetEntity(d => d.ID == DataID);
                if (SR.status == "3")
                {
                    SR.status = "4";
                    new SZHL_JH_ITEMZCB().Update(SR);
                }

            }
        }

        /// <summary>
        /// 恢复上报状态
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void HFZC(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int[] ListData = P1.TrimEnd(',').SplitTOInt(',');
            foreach (int DataID in ListData)
            {
                SZHL_JH_ITEMZC SR = new SZHL_JH_ITEMZCB().GetEntity(d => d.ID == DataID);
                if (SR.status == "4")
                {
                    SR.status = "3";
                    new SZHL_JH_ITEMZCB().Update(SR);
                }

            }
        }
        #endregion


    }
}