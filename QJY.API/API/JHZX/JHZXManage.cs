﻿using FastReflectionLib;
using Newtonsoft.Json;
using QJY.API;
using QJY.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;

namespace QJY.API
{
    public class JHZXManage : IWsService
    {

        public void ProcessRequest(HttpContext context, ref Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            MethodInfo methodInfo = typeof(JHZXManage).GetMethod(msg.Action.ToUpper());
            JHZXManage model = new JHZXManage();
            methodInfo.FastInvoke(model, new object[] { context, msg, P1, P2, UserInfo });
        }





        public void GETZXINITDATA(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int Id = int.Parse(P1);
            int MXId = int.Parse(P2);

            msg.Result = new SZHL_JHB().GetEntity(D => D.ID == Id); ;
            msg.Result1 = new VwSRB().GetEntity(D => D.ID == MXId);
            msg.Result2 = new SZHL_JH_ITEMZXB().GetEntities(d => d.szid == MXId && d.sztype == "0");//执行记录
            msg.Result3 = new JH_Auth_ZiDianB().GetEntities(d => d.Class == 30 && d.Remark != "1").OrderBy(D => D.TypeNO);//产品类型
            msg.Result4 = new JH_Auth_ZiDianB().GetEntities(d => d.Class == 31 && d.Remark != "1");//产品类型


        }
        public void ADD(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            SZHL_JH_ITEMZX MODEL = JsonConvert.DeserializeObject<SZHL_JH_ITEMZX>(P1);
            MODEL.CRDate = DateTime.Now;
            MODEL.CRUser = UserInfo.User.UserName;
            MODEL.ComID = UserInfo.User.ComId;
            MODEL.sztype = "0";
            new SZHL_JH_ITEMZXB().Insert(MODEL);


            SZHL_JH_ITEMSR SRMODEL = new SZHL_JH_ITEMSRB().GetEntity(D => D.ID == MODEL.szid);
            SRMODEL.zxje = SRMODEL.zxje + MODEL.jsje;
            if (SRMODEL.zxje >= SRMODEL.je1)
            {
                SRMODEL.status = "5";
            }//金额超过了更改状态为已执行完毕
            new SZHL_JH_ITEMSRB().Update(SRMODEL);

            msg.Result = MODEL;
        }


        public void DEL(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int Id = int.Parse(P1);
            SZHL_JH_ITEMZX MODEL = new SZHL_JH_ITEMZXB().GetEntity(D => D.ID == Id);

            SZHL_JH_ITEMSR SRMODEL = new SZHL_JH_ITEMSRB().GetEntity(D => D.ID == MODEL.szid);
            SRMODEL.zxje = SRMODEL.zxje - MODEL.jsje;
            if (SRMODEL.zxje < SRMODEL.je1)
            {
                SRMODEL.status = "3";
            }//金额超过了更改状态为未执行完毕
            new SZHL_JH_ITEMSRB().Update(SRMODEL);
            new SZHL_JH_ITEMZXB().Delete(d => d.ID == Id);
        }




        /// <summary>
        /// 获取已上报(可执行的计划明细)
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETJHMXLIST(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int Id = int.Parse(P1);
            List<int> ListDwids = UserInfo.UserBMQXCode.SplitTOListint(',');

            msg.Result = new VwSRB().GetEntities(d => d.JHID == Id && (d.status == P2) && ListDwids.Contains(d.dwid.Value));
        }


        public void UPZXZT(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int Id = int.Parse(P1);
            //审核结束
            SZHL_JH_ITEMSR ZC = new SZHL_JH_ITEMSRB().GetEntity(d => d.ID == Id);
            ZC.status = (P2 == "Y" ? "5" : "3"); //回到已上报状态
            new SZHL_JH_ITEMSRB().Update(ZC);

        }



    }
}