﻿using QJY.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using FastReflectionLib;
using System.Data;
using QJY.Data;
using Newtonsoft.Json;

namespace QJY.API
{
    public class ZCZXManage : IWsService
    {

        public void ProcessRequest(HttpContext context, ref Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            MethodInfo methodInfo = typeof(ZCZXManage).GetMethod(msg.Action.ToUpper());
            ZCZXManage model = new ZCZXManage();
            methodInfo.FastInvoke(model, new object[] { context, msg, P1, P2, UserInfo });
        }








        /// <summary>
        /// 获取已上报(可执行的计划明细)
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETJHMXLIST(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int Id = int.Parse(P1);
            msg.Result = new SZHL_JH_ITEMSRB().GetEntities(d => d.JHID == Id && (d.status == "3" || d.status == "4"));
        }




        /// <summary>
        /// 获取待交单和已交单的数据
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETJDMXLIST(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int Id = int.Parse(P1);
            List<int> ListDwids = UserInfo.UserBMQXCode.SplitTOListint(',');

            if (P2 == "0")
            {
                msg.Result = new VwZCB().GetEntities(d => d.JHID == Id && (d.status == "3"));
            }
            else
            {
                msg.Result = new VwZCB().GetEntities(d => d.JHID == Id && d.jdr == UserInfo.User.UserName);
            }
        }




        public void GETSDMXLIST(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int Id = int.Parse(P1);
            List<int> ListDwids = UserInfo.UserBMQXCode.SplitTOListint(',');
            if (P2 == "0")
            {
                msg.Result = new VwZCB().GetEntities(d => d.JHID == Id && (d.status == "5"));
            }
            else
            {
                msg.Result = new VwZCB().GetEntities(d => d.JHID == Id && d.sdr == UserInfo.User.UserName);
            }
        }

        /// <summary>
        /// 交单初始化
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETJDINITDATA(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int Id = int.Parse(P1);
            int MXId = int.Parse(P2);

            msg.Result = new SZHL_JHB().GetEntity(D => D.ID == Id); ;
            msg.Result1 = new VwZCB().GetEntity(D => D.ID == MXId);
        }



        /// <summary>
        /// 交单
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void ZCJD(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            int[] ListData = P1.TrimEnd(',').SplitTOInt(',');
            decimal jdje = decimal.Parse(context.Request["jdje"].ToString());
            string strkhh = context.Request["khh"].ToString();
            string strlhh = context.Request["lhh"].ToString();
            string strskdw = context.Request["skdw"].ToString();
            string strskzh = context.Request["skzh"].ToString();

            foreach (int DataID in ListData)
            {
                SZHL_JH_ITEMZC ZC = new SZHL_JH_ITEMZCB().GetEntity(d => d.ID == DataID);
                if (ZC.status == "3")
                {
                    ZC.status = "5";
                    ZC.jdr = UserInfo.User.UserName;
                    ZC.jddate = DateTime.Now;
                    ZC.jdje = jdje;
                    ZC.khhlhh = strlhh;
                    ZC.skdw = strskdw;
                    ZC.skkhh = strkhh;
                    ZC.skzh = strskzh;
                    new SZHL_JH_ITEMZCB().Update(ZC);
                }

            }
        }




        /// <summary>
        /// 收单（需要指出是否走付款一览表流程）
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void ZCSD(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strisylb = context.Request["isylb"].ToString();

            int[] ListData = P1.TrimEnd(',').SplitTOInt(',');
            foreach (int DataID in ListData)
            {
                SZHL_JH_ITEMZC SR = new SZHL_JH_ITEMZCB().GetEntity(d => d.ID == DataID);
                if (SR.status == "5")
                {
                    SR.status = "6";
                    if (strisylb != "Y")
                    {
                        SR.status = "8";
                    }
                    SR.sdr = UserInfo.User.UserName;
                    SR.sddate = DateTime.Now;
                    new SZHL_JH_ITEMZCB().Update(SR);
                }

            }
        }



        public void JJSD(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int[] ListData = P1.TrimEnd(',').SplitTOInt(',');
            foreach (int DataID in ListData)
            {
                SZHL_JH_ITEMZC SR = new SZHL_JH_ITEMZCB().GetEntity(d => d.ID == DataID);
                if (SR.status == "5")//状态为已交单的时候可以拒绝收单
                {
                    SR.status = "-2";
                    SR.statusremark = P2;
                    SR.sdr = UserInfo.User.UserName;
                    SR.sddate = DateTime.Now;
                    new SZHL_JH_ITEMZCB().Update(SR);
                }

            }
        }




        /// <summary>
        /// 获取可提交一览表的支出明细
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETYLBMXLIST(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int Id = int.Parse(P1);
            List<int> ListDwids = UserInfo.UserBMQXCode.SplitTOListint(',');

            if (P2 == "0")
            {
                msg.Result = new VwZCB().GetEntities(d => d.JHID == Id && (d.status == "6"));
            }
            else
            {
                msg.Result = new VwZCB().GetEntities(d => d.JHID == Id && d.ylbuser == UserInfo.User.UserName);
            }
        }




        /// <summary>
        /// 初始化付款一览表
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETYLBINITDATA(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strSQL = string.Format(" SELECT * FROM VwZC WHERE 1=1 ");
            strSQL += " And ID in ('" + P1.ToFormatLike(',') + "')";
            msg.Result = new SZHL_JH_ITEMZCB().GetDTByCommand(strSQL);
        }

        public void GETMODEL(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int Id = int.Parse(P1);
            msg.Result = new SZHL_JH_YLBB().GetEntity(D => D.ID == Id);

        }

        /// <summary>
        /// 添加付款一览表
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1">客户信息</param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void ADDFKYLB(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            SZHL_JH_YLB MODEL = JsonConvert.DeserializeObject<SZHL_JH_YLB>(P1);

            List<SZHL_JH_ITEMZC> MODELS = JsonConvert.DeserializeObject<List<SZHL_JH_ITEMZC>>(P2);


            if (MODEL.ID == 0)
            {
                MODEL.CRDate = DateTime.Now;
                MODEL.CRUser = UserInfo.User.UserName;
                MODEL.ComId = UserInfo.User.ComId;
                MODEL.ShenQingRen = UserInfo.User.UserRealName;
                MODEL.BranchNo = UserInfo.BranchInfo.DeptCode;
                new SZHL_JH_YLBB().Insert(MODEL);

                foreach (var jhmxid in MODELS)
                {
                    //审核中
                    SZHL_JH_ITEMZC ZC = new SZHL_JH_ITEMZCB().GetEntity(d => d.ID == jhmxid.ID);
                    ZC.status = "7";
                    ZC.ylbdate = DateTime.Now;
                    ZC.ylbuser = UserInfo.User.UserName;
                    ZC.ylbje = jhmxid.ylbje;
                    new SZHL_JH_ITEMZCB().Update(ZC);
                }

            }
            else
            {
                new SZHL_JH_YLBB().Update(MODEL);
            }
            msg.Result = MODEL;
        }

        public void COMPLTE(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            SZHL_JH_YLB MODEL = JsonConvert.DeserializeObject<SZHL_JH_YLB>(P1);
            foreach (int jhmxid in MODEL.JHMXID.SplitTOInt(','))
            {
                //审核结束
                SZHL_JH_ITEMZC ZC = new SZHL_JH_ITEMZCB().GetEntity(d => d.ID == jhmxid);
                ZC.status = "8";//
                new SZHL_JH_ITEMZCB().Update(ZC);
            }

        }
        public void REBACK(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            SZHL_JH_YLB MODEL = JsonConvert.DeserializeObject<SZHL_JH_YLB>(P1);
            foreach (int jhmxid in MODEL.JHMXID.SplitTOInt(','))
            {
                //审核结束
                SZHL_JH_ITEMZC ZC = new SZHL_JH_ITEMZCB().GetEntity(d => d.ID == jhmxid);
                ZC.status = "6"; //回到收单状态
                new SZHL_JH_ITEMZCB().Update(ZC);
            }

        }

        /// <summary>
        /// 更新执行状态
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>

        public void UPZXZT(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int Id = int.Parse(P1);
            //审核结束
            SZHL_JH_ITEMZC ZC = new SZHL_JH_ITEMZCB().GetEntity(d => d.ID == Id);
            ZC.status =( P2 == "Y" ? "9" : "8"); //回到收单状态
            new SZHL_JH_ITEMZCB().Update(ZC);

        }


        /// <summary>
        /// 一览表审核查询
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETJHYLBLIST(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string userName = UserInfo.User.UserName;
            string strWhere = "";
            if (P1 != "")
            {
                DataTable dt = new DataTable();
                switch (P1)
                {

                    case "0": //创建的。
                        {
                            strWhere += " And SZHL_JH_YLB.CRUser ='" + userName + "'";
                        }
                        break;
                    case "1": //待审核
                        {
                            var intProD = new Yan_WF_PIB().GetDSH(UserInfo.User).Select(d => d.PIID.ToString()).ToList();
                            if (intProD.Count > 0)
                            {
                                strWhere += " And SZHL_JH_YLB.intProcessStanceid in (" + (intProD.ListTOString(',') == "" ? "0" : intProD.ListTOString(',')) + ")";
                            }
                            else
                            {
                                strWhere += " And 1=0";
                            }
                        }
                        break;
                    case "2":  //已审核
                        {
                            var intProD = new Yan_WF_PIB().GetYSH(UserInfo.User).Select(d => d.PIID.ToString()).ToList();
                            if (intProD.Count > 0)
                            {
                                strWhere += " And SZHL_JH_YLB.intProcessStanceid in (" + (intProD.ListTOString(',') == "" ? "0" : intProD.ListTOString(',')) + ")";

                            }
                            else
                            {
                                strWhere += " And 1=0";
                            }
                        }
                        break;
                }
                dt = new SZHL_JH_BGB().GetDTByCommand(" SELECT *   FROM SZHL_JH_YLB  WHERE   SZHL_JH_YLB.BranchNO in ('" + UserInfo.UserBMQXCode.ToFormatLike(',') + "')  " + strWhere);
                msg.Result = dt;
            }
        }



        /// <summary>
        /// 提交一览表
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void ZCYLB(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int[] ListData = P1.TrimEnd(',').SplitTOInt(',');

            JH_Auth_QY_Model Model = new JH_Auth_QY_ModelB().GetEntity(d => d.QYModelCode == "ZCZX");
            int PDID = 0;
            int.TryParse(Model.PDID.ToString(), out PDID);
            Yan_WF_PD PD = new Yan_WF_PDB().GetEntity(d => d.ID == PDID && d.ComId == UserInfo.User.ComId);
            Yan_WF_PIB PIB = new Yan_WF_PIB();
            List<string> ListNextUser = new List<string>();//获取下一任务的处理人
            foreach (int DataID in ListData)
            {
                Yan_WF_TI TI = PIB.StartWF(PD, "ZCZX", UserInfo.User.UserName, "", "", ref ListNextUser);
                //更新关联表的流程ID

                SZHL_JH_ITEMZC SR = new SZHL_JH_ITEMZCB().GetEntity(d => d.ID == DataID);
                if (SR.status == "6")
                {
                    SR.ylbuser = UserInfo.User.UserName;
                    SR.ylbdate = DateTime.Now;
                    SR.ylbstatus = "1";
                    SR.ylbintProcessStanceid = TI.PIID;
                    new SZHL_JH_ITEMZCB().Update(SR);
                }
            }

        }

        /// <summary>
        ///获取已填写的收款单位
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETSKDWLISTPAGE(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string userName = UserInfo.User.UserName;
            string strWhere = "";
            if (P1 != "")
            {
                strWhere += string.Format(" And  SKDW like '%{0}%'", P1);
            }

            int page = 0;
            int pagecount = 8;
            int.TryParse(context.Request["p"] ?? "1", out page);
            int.TryParse(context.Request["pagecount"] ?? "8", out pagecount);//页数
            page = page == 0 ? 1 : page;
            int total = 0;
            DataTable dt = new SZHL_JH_ITEMZCB().GetDataPager("( SELECT DISTINCT  SKDW,skkhh,khhlhh,skzh FROM  SZHL_JH_ITEMZC WHERE skdw IS NOT NULL ) A  ", "   SKDW,skkhh,khhlhh,skzh ", pagecount, page, " SKDW  ", strWhere, ref total);
            msg.Result = dt;
            msg.Result1 = total;
        }

        /// <summary>
        /// 获取审核的支出一览表
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETSHMXLIST(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strSQL = string.Format(" SELECT * FROM VwZC WHERE JHID='{0}' ", P1);
            List<string> intProD = new List<string>();
            if (P2 == "0")//待审核
            {
                intProD = new Yan_WF_PIB().GetDSH(UserInfo.User).Select(d => d.PIID.ToString()).ToList();
            }
            else
            {
                intProD = new Yan_WF_PIB().GetYSH(UserInfo.User).Select(d => d.PIID.ToString()).ToList();

            }

            strSQL += " And VwZC.ylbintProcessStanceid in (" + (intProD.ListTOString(',') == "" ? "-1" : intProD.ListTOString(',')) + ")";
            msg.Result = new SZHL_JH_ITEMZCB().GetDTByCommand(strSQL);

        }


        /// <summary>
        /// 审核支出一览表
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void SHYLB(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int[] ListData = P1.TrimEnd(',').SplitTOInt(',');
            foreach (int DataID in ListData)
            {
                SZHL_JH_ITEMZC SR = new SZHL_JH_ITEMZCB().GetEntity(d => d.ID == DataID);
                Yan_WF_PIB PIB = new Yan_WF_PIB();
                List<string> ListNextUser = new List<string>();
                PIB.MANAGEWF(UserInfo.User.UserName, SR.ylbintProcessStanceid.Value, "审核通过", ref ListNextUser, "");//处理任务
                string strIsComplete = ListNextUser.Count() == 0 ? "Y" : "N";//结束流程,找不到人了
                if (strIsComplete == "Y")//找不到下家就结束流程,并且给流程发起人发送消息
                {
                    PIB.ENDWF(SR.ylbintProcessStanceid.Value);
                    SR.ylbstatus = "2";
                    new SZHL_JH_ITEMZCB().Update(SR);

                }
            }
        }


        /// <summary>
        /// 退回支出一览表
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void THYLB(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int[] ListData = P1.TrimEnd(',').SplitTOInt(',');
            foreach (int DataID in ListData)
            {
                SZHL_JH_ITEMZC SR = new SZHL_JH_ITEMZCB().GetEntity(d => d.ID == DataID);
                Yan_WF_PIB PIB = new Yan_WF_PIB();
                List<string> ListNextUser = new List<string>();
                new Yan_WF_PIB().REBACKLC(UserInfo.User.UserName, SR.ylbintProcessStanceid.Value, P2);//退回任务
                SR.ylbstatus = "-1";
                SR.ylbstatusremark = P2;
                new SZHL_JH_ITEMZCB().Update(SR);

            }
        }






        /// <summary>
        /// 获取可执行支出明细
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETZXMXLIST(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int Id = int.Parse(P1);
            msg.Result = new VwZCB().GetEntities(d => d.JHID == Id && d.status == P2);

        }







        #region 支出执行
        public void GETZXINITDATA(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int Id = int.Parse(P1);
            int MXId = int.Parse(P2);

            msg.Result = new SZHL_JHB().GetEntity(D => D.ID == Id); ;
            msg.Result1 = new VwZCB().GetEntity(D => D.ID == MXId);
            msg.Result2 = new SZHL_JH_ITEMZXB().GetEntities(d => d.szid == MXId && d.sztype == "1");//执行记录
            msg.Result3 = new JH_Auth_ZiDianB().GetEntities(d => d.Class == 30 && d.Remark != "1").OrderBy(D => D.TypeNO);//产品类型
            msg.Result4 = new JH_Auth_ZiDianB().GetEntities(d => d.Class == 31 && d.Remark != "1");//产品类型


        }
        public void ADD(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            SZHL_JH_ITEMZX MODEL = JsonConvert.DeserializeObject<SZHL_JH_ITEMZX>(P1);

            MODEL.CRDate = DateTime.Now;
            MODEL.CRUser = UserInfo.User.UserName;
            MODEL.ComID = UserInfo.User.ComId;
            MODEL.sztype = "1";
            new SZHL_JH_ITEMZXB().Insert(MODEL);



            SZHL_JH_ITEMZC SRMODEL = new SZHL_JH_ITEMZCB().GetEntity(D => D.ID == MODEL.szid);
            SRMODEL.zxje = SRMODEL.zxje + MODEL.jsje;
            if (SRMODEL.zxje >= SRMODEL.je1)
            {
                SRMODEL.status = "9";
            }//金额超过了更改状态为已执行完毕
            new SZHL_JH_ITEMZCB().Update(SRMODEL);


            msg.Result = MODEL;
        }



        public void DEL(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int Id = int.Parse(P1);

            SZHL_JH_ITEMZX MODEL = new SZHL_JH_ITEMZXB().GetEntity(D => D.ID == Id);

            SZHL_JH_ITEMZC SRMODEL = new SZHL_JH_ITEMZCB().GetEntity(D => D.ID == MODEL.szid);
            SRMODEL.zxje = SRMODEL.zxje - MODEL.jsje;
            if (SRMODEL.zxje < SRMODEL.je1)
            {
                SRMODEL.status = "8";
            }//金额超过了更改状态为未执行完毕
            new SZHL_JH_ITEMZCB().Update(SRMODEL);

            new SZHL_JH_ITEMZXB().Delete(d => d.ID == Id);
        }

        #endregion

    }
}