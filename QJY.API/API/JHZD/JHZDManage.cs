﻿using QJY.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using FastReflectionLib;
using System.Data;
using QJY.Data;
using Newtonsoft.Json;

namespace QJY.API
{
    public class JHZDManage : IWsService
    {

        public void ProcessRequest(HttpContext context, ref Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            MethodInfo methodInfo = typeof(JHZDManage).GetMethod(msg.Action.ToUpper());
            JHZDManage model = new JHZDManage();
            methodInfo.FastInvoke(model, new object[] { context, msg, P1, P2, UserInfo });
        }

        /// <summary>
        /// 获取记事本
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1">类型</param>
        /// <param name="P2">查询条件</param>
        /// <param name="strUserName"></param>
        public void GETJHSXLIST(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            if (P1 == "")
            {
                msg.Result = new SZHL_JH_FLB().GetALLEntities();

            }
            else
            {
                msg.Result = new SZHL_JH_FLB().GetEntities(D => D.name.Contains(P1));

            }
            msg.Result1 = new JH_Auth_ZiDianB().GetEntities(D => D.Class == 25);
        }


        public void GETALLJHSXTREE(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            //判断是否强制加载所有部门数据
            DataTable dtBMS = new DataTable();
            int deptRoot = -1;
            //获取有权限的部门Id

            string strUserTree = "";
            if (P2 == "Y")
            {
                strUserTree = "[" + new SZHL_JH_FLB().GetJHTree(deptRoot, UserInfo.User.ComId.Value, P1).TrimEnd(',') + "]";
            }
            if (P2 == "N")
            {
                strUserTree = "[" + new SZHL_JH_FLB().GetJHZCTree(deptRoot, UserInfo.User.ComId.Value, P1).TrimEnd(',') + "]";
            }
            msg.Result = strUserTree;
            msg.Result1 = UserInfo.User.BranchCode;
        }


        /// <summary>
        /// 添加记事本
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="strUserName"></param>
        public void ADDJHSX(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            SZHL_JH_FL MODEL = JsonConvert.DeserializeObject<SZHL_JH_FL>(P1);

            if (MODEL.name == null)
            {
                msg.ErrorMsg = "内容不能为空";
                return;
            }
            string kmdm = new SZHL_JH_FLB().GetKMDM(MODEL.pid);

            if (MODEL.ID == 0)
            {
                MODEL.path = kmdm;
                MODEL.CRDate = DateTime.Now;
                MODEL.CRUser = UserInfo.User.UserName;
                MODEL.ComID = UserInfo.User.ComId;
                new SZHL_JH_FLB().Insert(MODEL);

            }
            else
            {
                MODEL.path = kmdm;
                new SZHL_JH_FLB().Update(MODEL);
            }

            msg.Result = MODEL;
        }

        /// <summary>
        /// 删除记事本
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1">日报ID</param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void DELJHSXBYID(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            try
            {
                if (new SZHL_JH_FLB().Delete(d => d.ID.ToString() == P1))
                {
                    msg.ErrorMsg = "";
                }
            }
            catch (Exception ex)
            {
                msg.ErrorMsg = ex.Message;
            }
        }

        public void GETJHSXMODEL(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int Id = 0;
            int.TryParse(P1, out Id);
            SZHL_JH_FL sg = new SZHL_JH_FLB().GetEntity(d => d.ID == Id && d.ComID == UserInfo.User.ComId);
            msg.Result = sg;
        }

    }
}