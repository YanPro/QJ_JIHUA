﻿using QJY.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using FastReflectionLib;
using System.Data;
using QJY.Data;
using Newtonsoft.Json;

namespace QJY.API
{
    public class JHBGManage : IWsService
    {

        public void ProcessRequest(HttpContext context, ref Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            MethodInfo methodInfo = typeof(JHBGManage).GetMethod(msg.Action.ToUpper());
            JHBGManage model = new JHBGManage();
            methodInfo.FastInvoke(model, new object[] { context, msg, P1, P2, UserInfo });
        }





        #region 计划管理



        /// <summary>
        /// 请假列表
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETJHBGLIST(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string userName = UserInfo.User.UserName;
            string strWhere = "";
            if (P1 != "")
            {
                DataTable dt = new DataTable();
                switch (P1)
                {

                    case "0": //创建的
                        {
                            strWhere += " And SZHL_JH_BG.CRUser ='" + userName + "'";
                        }
                        break;
                    case "1": //待审核
                        {
                            var intProD = new Yan_WF_PIB().GetDSH(UserInfo.User).Select(d => d.PIID.ToString()).ToList();
                            if (intProD.Count > 0)
                            {
                                strWhere += " And SZHL_JH_BG.intProcessStanceid in (" + (intProD.ListTOString(',') == "" ? "0" : intProD.ListTOString(',')) + ")";
                            }
                            else
                            {
                                strWhere += " And 1=0";
                            }
                        }
                        break;
                    case "2":  //已审核
                        {
                            var intProD = new Yan_WF_PIB().GetYSH(UserInfo.User).Select(d => d.PIID.ToString()).ToList();
                            if (intProD.Count > 0)
                            {
                                strWhere += " And SZHL_JH_BG.intProcessStanceid in (" + (intProD.ListTOString(',') == "" ? "0" : intProD.ListTOString(',')) + ")";

                            }
                            else
                            {
                                strWhere += " And 1=0";
                            }
                        }
                        break;
                }
                dt = new SZHL_JH_BGB().GetDTByCommand(" SELECT SZHL_JH_BG.*,VwZC.dwname,VwZC.Remark1 AS XM,VwZC.Remark2 AS QB, VwZC.FLName,VwZC.htlb ,VwZC.htbh,VwZC.dfdw ,VwZC.htnr ,VwZC.je1 FROM SZHL_JH_BG LEFT JOIN VwZC ON SZHL_JH_BG.JHMXID=VwZC.ID WHERE 1=1" + strWhere);
                msg.Result = dt;
            }
        }



        public void GETJHZCMXLIST(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            int Id = int.Parse(P1);
            if (P2 == "")
            {
                msg.Result = new VwZCB().GetEntities(d => d.JHID == Id && d.status != "0");
            }
            else
            {
                msg.Result = new VwZCB().GetEntities(d => d.JHID == Id && d.status == P2);
            }
        }

        public void GETMXINITDATA(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            int Id = int.Parse(P1);
            VwZC MODEL = new VwZCB().GetEntity(d => d.ID == Id);
            msg.Result = MODEL;
            msg.Result1 = new SZHL_JHB().GetEntities(d => d.ID == MODEL.JHID);


        }
        public void GETMODEL(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            int Id = int.Parse(P1);
            msg.Result = new SZHL_JH_BGB().GetEntities(d => d.ID == Id).FirstOrDefault();

        }

        public void COM(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            int Id = int.Parse(P1);

            SZHL_JH_BG MODEL = new SZHL_JH_BGB().GetEntities(d => d.ID == Id).FirstOrDefault();


            SZHL_JH_ITEMZC ZC = new SZHL_JH_ITEMZCB().GetEntities(d => d.ID == MODEL.JHMXID).FirstOrDefault();
            ZC.lsje = ZC.je1;
            ZC.je1 =decimal.Parse( MODEL.BGJE);
            new SZHL_JH_ITEMZCB().Update(ZC);
        }
        


        public void ADDJHBG(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

      
            SZHL_JH_BG BG = JsonConvert.DeserializeObject<SZHL_JH_BG>(P1);
            if (BG.ID == 0)
            {

                SZHL_JH_ITEMZC MODEL = JsonConvert.DeserializeObject<SZHL_JH_ITEMZC>(BG.Remark2);
                if (MODEL.remark1 == "040101" || MODEL.remark1.StartsWith("0303") || MODEL.remark1.StartsWith("0304"))
                {
                    if (new SZHL_JH_ITEMZCB().GetEntities(d => d.htbh == MODEL.htbh && d.htjdid == MODEL.htjdid).Count() > 0)
                    {
                        msg.ErrorMsg = "已经存在相同的合同编号及节点";
                    }
                    else
                    {
                        BG.ShenQingRen = string.IsNullOrEmpty(BG.ShenQingRen) ? UserInfo.User.UserRealName : BG.ShenQingRen;
                        BG.CRDate = DateTime.Now;
                        BG.CRUser = UserInfo.User.UserName;
                        BG.BranchName = UserInfo.BranchInfo.DeptName;
                        BG.BranchNo = UserInfo.User.BranchCode;
                        BG.ComId = UserInfo.User.ComId;
                        new SZHL_JH_BGB().Insert(BG);
                    }
                }
                else
                {
                    BG.ShenQingRen = string.IsNullOrEmpty(BG.ShenQingRen) ? UserInfo.User.UserRealName : BG.ShenQingRen;
                    BG.CRDate = DateTime.Now;
                    BG.CRUser = UserInfo.User.UserName;
                    BG.BranchName = UserInfo.BranchInfo.DeptName;
                    BG.BranchNo = UserInfo.User.BranchCode;
                    BG.ComId = UserInfo.User.ComId;
                    new SZHL_JH_BGB().Insert(BG);
                }
            }
            else
            {
                new SZHL_JH_BGB().Update(BG);
            }
            msg.Result = BG;
        }

        public void ADDJHMXZC(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {


            int Id = int.Parse(P1);
            SZHL_JH_BG BG = new SZHL_JH_BGB().GetEntities(d => d.ID == Id).FirstOrDefault();
            SZHL_JH_ITEMZC MODEL = JsonConvert.DeserializeObject<SZHL_JH_ITEMZC>(BG.Remark2);
            if (MODEL.ID == 0)
            {

                    MODEL.status = "3";
                    MODEL.CRDate = DateTime.Now;
                    MODEL.CRUser = UserInfo.User.UserName;
                    MODEL.ComID = UserInfo.User.ComId;
                    MODEL.ylbstatus = "0";
                    new SZHL_JH_ITEMZCB().Insert(MODEL);
         
            }
            else
            {
                new SZHL_JH_ITEMZCB().Update(MODEL);
            }
            msg.Result = MODEL;
        }



        #endregion







    }
}